// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;

public class DriveUtil extends SubsystemBase {
  // NavX MXP Stuff
  AHRS navX;
  
  // Motor controllers
  private WPI_TalonSRX leftPrimary, leftSecondary, rightPrimary, rightSecondary;
  private Encoder leftEncoder, rightEncoder;

  // Speed variables
  private double speed, rotation, gyroAngle, navXAngle;

  // Drive controller
  private DifferentialDrive differentialDrive;

  // Shuffleboard Objects
  private ShuffleboardTab tab = Shuffleboard.getTab("Stronghold Diagnostics");
  private ShuffleboardLayout toggleButtons, motorControl, motorStats, motorSettings, idleMode;
  private NetworkTableEntry zeroAllMotors, leftSpeed, rightSpeed, combinedSpeed, speedLimit, joined, useController, leftTalonSpeed, rightTalonSpeed, leftEncoderValue, rightEncoderValue, idleModeDisplay, toggleIdleMode;

  /** Creates a new DriveUtil. */
  public DriveUtil() {
    // Shuffleboard Config
    toggleButtons = tab.getLayout("DriveUtil: Global Settings", BuiltInLayouts.kList).withSize(3, 2).withPosition(0, 0).withProperties(Map.of("Label position", "HIDDEN"));
    zeroAllMotors = toggleButtons.add("Zero Drive Motors", false).withWidget(BuiltInWidgets.kToggleButton).getEntry();
    joined = toggleButtons.add("Join Sides", true).withWidget(BuiltInWidgets.kToggleButton).getEntry();
    useController = toggleButtons.add("Use Controller", true).withWidget(BuiltInWidgets.kToggleButton).getEntry();
    motorControl = tab.getLayout("DriveUtil: Motor Control", BuiltInLayouts.kList).withSize(3, 4).withPosition(0, 2);
    leftSpeed = motorControl.add("Left Drive Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    rightSpeed = motorControl.add("Right Drive Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    combinedSpeed = motorControl.add("Drive Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    motorStats = tab.getLayout("DriveUtil: Motor Statistics", BuiltInLayouts.kList).withPosition(3, 0).withSize(3, 4);
    leftTalonSpeed = motorStats.add("Left Motor Speeds", 0.0).withWidget(BuiltInWidgets.kNumberBar).getEntry();
    rightTalonSpeed = motorStats.add("Right Motor Speeds", 0.0).withWidget(BuiltInWidgets.kNumberBar).getEntry();
    motorSettings = tab.getLayout("DriveUtil: Motor Settings", BuiltInLayouts.kList).withPosition(3, 4).withSize(3, 2);
    speedLimit = motorSettings.add("Speed Limit", 1.0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    idleMode = motorSettings.getLayout("Idle Mode", BuiltInLayouts.kList).withProperties(Map.of("Label position", "HIDDEN"));
    idleModeDisplay = idleMode.add("Idle Mode", "Coast").withWidget(BuiltInWidgets.kTextView).getEntry();

    // Initialize speed controllers (TalonSRX)
    leftPrimary = new WPI_TalonSRX(Constants.driveMotor_LeftPrimary);
    leftSecondary = new WPI_TalonSRX(Constants.driveMotor_LeftSecondary);
    rightPrimary = new WPI_TalonSRX(Constants.driveMotor_RightPrimary);
    rightSecondary = new WPI_TalonSRX(Constants.driveMotor_RightSecondary);

    leftPrimary.setNeutralMode(NeutralMode.Coast);
    rightPrimary.setNeutralMode(NeutralMode.Coast);
    leftSecondary.setNeutralMode(NeutralMode.Coast);
    rightSecondary.setNeutralMode(NeutralMode.Coast);

    // Set secondaries to follow primaries
    leftSecondary.follow(leftPrimary);
    rightSecondary.follow(rightPrimary);

    // Invert motors
    leftPrimary.setInverted(true);
    leftSecondary.setInverted(true);
    // rightPrimary.setInverted(true);
    // rightSecondary.setInverted(true);

    // Initialize DifferentialDrive controller
    differentialDrive = new DifferentialDrive(leftPrimary, rightPrimary);
  }

  public void diagnosticDrive() {
    if (joined.getBoolean(false) == true) {
      differentialDrive.tankDrive(combinedSpeed.getDouble(0.0) * speedLimit.getDouble(1.0), combinedSpeed.getDouble(0.0) * speedLimit.getDouble(1.0));
      leftSpeed.setDouble(combinedSpeed.getDouble(0.0));
      rightSpeed.setDouble(combinedSpeed.getDouble(0.0));
    } else {
      differentialDrive.tankDrive(leftSpeed.getDouble(0.0) * speedLimit.getDouble(1.0), rightSpeed.getDouble(0.0) * speedLimit.getDouble(1.0));
      combinedSpeed.setDouble((leftSpeed.getDouble(0.0) + rightSpeed.getDouble(0.0))/2);
    }
  }

  public void driveRobot() {
    speed = RobotContainer.getDriverLeftTrigger() - RobotContainer.getDriverRightTrigger();
    rotation = RobotContainer.getDriverLeftJoystickX();
    differentialDrive.arcadeDrive(speed * speedLimit.getDouble(1.0), rotation * speedLimit.getDouble(1.0), true);
    leftSpeed.setDouble(0.0);
    rightSpeed.setDouble(0.0);
    combinedSpeed.setDouble(0.0);
  }

  public void zeroAll(){
    if (zeroAllMotors.getBoolean(false) == true){
      differentialDrive.stopMotor();
      zeroAllMotors.setBoolean(false);
      leftSpeed.setDouble(0.0);
      rightSpeed.setDouble(0.0);
      combinedSpeed.setDouble(0.0);
    }
  }

  public void runMotors(){
    if (useController.getBoolean(true) == false){
      zeroAll();
      diagnosticDrive();
    } else {
      driveRobot();
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    leftTalonSpeed.setDouble(leftPrimary.get());
    rightTalonSpeed.setDouble(rightPrimary.get());
  }
}

